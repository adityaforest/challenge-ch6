const express = require('express')
const router = express.Router()
const userData = require('../services/user_db')
const cors = require('cors')

router.use(cors())
router.use(express.json())
router.use(express.urlencoded({ extended: true }))

router.get('/', async (req, res) => {    
    res.json(await userData.readAllUserData())
})

router.get('/:id', async (req, res) => {
    let id = parseInt(req.params.id)    
    res.json(await userData.readUserData(id))
})

router.post('/', async (req, res) => {
    let payload = {
        username: req.body.username,
        password: req.body.password,
        email: req.body.email,
        firstName: req.body.firstName,
        lastName: req.body.lastName
    }
    await userData.createUserData(payload)
    res.status(201).json({
        signupSuccess:true
    })    
})

router.put('/:id', async (req, res) => {
    id = parseInt(req.params.id)
    let payload = {
        username: req.body.username,
        password: req.body.password,
        email: req.body.email,
        firstName: req.body.firstName,
        lastName: req.body.lastName
    }
    await userData.updateUserData(id, payload)
    res.status(201).json({
        message: "data updated"
    })
})

router.delete('/:id', async (req, res) => {
    let id = parseInt(req.params.id)
    await userData.deleteUserData(id)
    res.status(201).json({
        message: "user data removed"
    })  
})

module.exports = router