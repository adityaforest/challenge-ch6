const express = require('express')
const router = express.Router()
const userHistory = require('../services/user_history')
const cors = require('cors')

router.use(cors())
router.use(express.json())
router.use(express.urlencoded({ extended: true }))

router.get('/', async (req, res) => {    
    res.json(await userHistory.readAllUserHistory())
})

router.get('/:id', async (req, res) => {
    let id = parseInt(req.params.id)    
    res.json(await userHistory.readUserHistoryByUserid(id))
})

router.post('/', async (req, res) => {
    let payload = {
        userid: req.body.userid,
        result: req.body.result,        
    }
    await userHistory.createUserHistory(payload)
    res.end()
})

router.delete('/:id', async (req, res) => {
    let id = parseInt(req.params.id)
    await userHistory.deleteUserHistory(id)
    res.status(201).json({
        message: "user data removed"
    })  
})

module.exports = router