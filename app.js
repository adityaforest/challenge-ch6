const express = require('express')
const app = express()
const port = 3000
const cookieParser = require("cookie-parser");
const sessions = require('express-session');
const userData = require('./services/user_db')

const path = require('path')
app.use('/public', express.static(path.join('./public')))

app.set('view engine', 'ejs')
app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.use(cookieParser())

//express session untuk user login
const oneDay = 1000 * 60 * 60 * 24;
app.use(sessions({
  secret: "123",
  saveUninitialized: true,
  cookie: { maxAge: oneDay },
  resave: false
}))
var session = {}
userAuthenticate = (req, res, next) => {
  if (!session.user) {
    res.redirect('/login')
  }
  else {
    session = req.session
    req.user = session.user
    next()
  }
}

refreshSession = (req, newUserData) => {
  session = req.session
  session.user = newUserData
}

const gamesRouter = require('./routes/games')
app.use('/games', userAuthenticate, gamesRouter)

const user = require('./api/user')
app.use('/api/v1/users', userAuthenticate, user)

const history = require('./api/history')
app.use('/api/v1/history', userAuthenticate, history)

//loggedIn var untuk dashboard login
let loggedin = false;

//ENDPOINTS FOR USER
app.get('/', (req, res) => {
  session = req.session
  if (session.user) {
    res.render('index', {
      user: session.user
    })
  } else {
    res.render('index', {
      user: false
    })
  }
})

app.get('/login', (req, res) => {
  res.render('login', {
    message: ''
  })
})

app.post('/login', async (req, res) => {
  // let user = JSON.stringify(await userData.readUserDataByUsername(req.body.username))
  let user = await userData.readUserDataByUsername(req.body.username)
  if(user != null){
    if (req.body.username == user.username && req.body.password == user.password) {
      refreshSession(req, user)
      if(user.id == 1){
        res.redirect('/dashboard')  
      }
      else{
        res.redirect('/')
      }    
    }
    else {
      res.render('login', { message: "Invalid username or password" })
    }
  }
  else{
    res.render('login', { message: "Invalid username or password" })
  }
})

app.get('/logout', (req, res) => {
  req.session.destroy()
  res.redirect('/')
})

app.get('/signup', (req, res) => {
  res.render('signup')
})

app.get('/myProfile/:id', userAuthenticate, async (req, res) => {
  let user = await userData.readUserData(parseInt(req.params.id))
  refreshSession(req, user)
  res.render('myProfile', {
    user: session.user
  })
})

//ENDPOINTS FOR DASHBOARD
app.get('/dashboard', userAuthenticate, async (req, res) => {
  if (req.user.id == 1) {
    res.render('dashboard')
  }
  else {
    res.json({ message: 'YOU MUST LOGIN AS ADMIN TO ACCESS DASHBOARD' })
  }
})

// app.get('/dashboard/login', async (req, res) => {
//   res.render('dashboard-login', {
//     message: ''
//   })
// })

// app.post('/dashboard/login', async (req, res) => {
//   let { username, password } = req.body
//   if (username == 'admin' && password == 'admin') {
//     loggedin = true
//     res.redirect('/dashboard')
//   }
//   else {
//     res.render('dashboard-login', {
//       message: 'Wrong username or password !'
//     })
//   }
// })

app.listen(port, () => {
  console.log(`Express running on port ${port}`)
})