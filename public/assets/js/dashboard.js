const BASE_URL = "http://localhost:3000"

//================================== USER SECTION START ================================================
const formUser = document.getElementById("user-form")
const formTitle = document.getElementById("form-title")
const username = document.getElementById("username")
const password = document.getElementById("password")
const email = document.getElementById("email")
const firstName = document.getElementById("firstName")
const lastName = document.getElementById("lastName")
const saveFormButton = document.getElementById('saveFormButton')
const listData = document.getElementById('list-data')
const delconfText = document.getElementById('delconf-text')
const editconfText = document.getElementById('editconf-text')

let clickedId = false

let oldData = {}

formUser.onsubmit = function (e) {
    e.preventDefault()
    saveData()
}

saveData = () => {
    let payload = {
        username: username.value,
        password: password.value,
        email: email.value,
        firstName: firstName.value,
        lastName: lastName.value
    }
    if (!clickedId) {
        fetch(`${BASE_URL}/api/v1/users`, {
            method: "POST",
            headers: {
                "content-type": "application/json"
            },
            body: JSON.stringify(payload)
        })
            .then(response => {
                loadData()
            })
    }
    else{
        editDataPrep2()
    }
}

loadData = () => {
    fetch(`${BASE_URL}/api/v1/users`)
        .then(res => res.json())
        .then(users => {
            listData.innerHTML = ""
            for (let i in users) {
                let user = users[i]
                listData.innerHTML += `
                        <div class="accordion-item">
                            <h2 class="accordion-header row" id="headingOne">
                                <div class="col-9">
                                    <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                        data-bs-target="#collapse${i}" aria-expanded="true"
                                        aria-controls="collapse${i}">
                                        <h5>id: ${user.id} username: ${user.username}
                                        </h5>
                                    </button>
                                </div>
                                <div class="col-3">
                                    <button class="btn btn-success btn-sm rounded-0 buttonEdit" type="button" 
                                        title="Edit" data-id="${user.id}" data-bs-toggle="modal" data-bs-target="#form-modal">
                                        <i class="fa fa-edit" data-id="${user.id}">
                                        </i></button>
                                    <button class="btn btn-danger btn-sm rounded-0 buttonDelete" type="button"
                                        title="Delete" data-id="${user.id}" data-bs-toggle="modal" data-bs-target="#delconf">
                                        <i class="fa fa-trash" data-id="${user.id}">
                                        </i></button>
                                </div>
                            </h2>
                            <div id="collapse${i}" class="accordion-collapse collapse" aria-labelledby="headingOne"
                                data-bs-parent="#accordionExample">
                                <div class="accordion-body row">
                                    <div class="col-6">
                                        <p>id : <div id="">${user.id}</div>
                                        </p>
                                        <p>username : <div id="username-id${user.id}">${user.username}</div>
                                        </p>
                                        <p>password : <div id="password-id${user.id}">${user.password}</div>
                                        </p>
                                    </div>
                                    <div class="col-6">
                                        <p>email : <div id="email-id${user.id}">${user.biodata.email}</div>
                                        </p>
                                        <p>first name : <div id="firstName-id${user.id}">${user.biodata.firstName}</div>
                                        </p>
                                        <p>last name : <div id="lastName-id${user.id}">${user.biodata.lastName}</div>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>`
            }
        })
}

openFormAsAdd = () => {
    clickedId = false
    formTitle.innerHTML = "ADD NEW USER"
    username.value = ''
    email.value = ''
    password.value = ''
    firstName.value = ''
    lastName.value = ''
    saveFormButton.removeAttribute('data-bs-toggle')
    saveFormButton.removeAttribute('data-bs-target')
}

openFormAsEdit = (user) => {
    clickedId = user.id
    username.value = user.username
    password.value = user.password
    firstName.value = user.biodata.firstName
    lastName.value = user.biodata.lastName
    email.value = user.biodata.email
    saveFormButton.setAttribute('data-bs-toggle',"modal")
    saveFormButton.setAttribute('data-bs-target',"#editconf")
}

deleteData = () => {
    if(clickedId == 1){
        alert(`You can't delete admin data !`)
        return
    }
    fetch(`${BASE_URL}/api/v1/users/${clickedId}`, {
        method: "DELETE"
    }).then(res => {
        loadData()
    })
}

listData.addEventListener('click', e => {
    if (e.target.classList.contains("buttonEdit") ||
        e.target.classList.contains("fa-edit")) {
        let id = e.target.getAttribute("data-id")
        e.preventDefault()             
        formTitle.innerHTML = `EDIT USER WITH ID:${id}`
        fetch(`${BASE_URL}/api/v1/users/${id}`)
            .then(res => res.json())
            .then(user => {
                editDataPrep1(user)
                openFormAsEdit(user)
            })
    }
    if (e.target.classList.contains("buttonDelete") ||
        e.target.classList.contains("fa-trash")) {
        let id = e.target.getAttribute("data-id")
        e.preventDefault()        
        clickedId = id
        fetch(`${BASE_URL}/api/v1/users/${id}`)
            .then(res => res.json())
            .then(user => {
                delconfText.innerHTML = `
                    <p>Are you sure want to delete this user ?</p>
                    <p>User ID : ${id}</p>
                    <p>Username : ${user.username}</p>
                `
            })

    }
})

editDataPrep1 = (oldDataInp) => {
    // console.log('edit prep 1')
    oldData = `
        <p>username: ${oldDataInp.username}</p>
        <p>password: ${oldDataInp.password}</p>
        <p>first name: ${oldDataInp.biodata.firstName}</p>
        <p>last name: ${oldDataInp.biodata.lastName}</p>
        <p>email: ${oldDataInp.biodata.email}</p>
    `
}

editDataPrep2 = () => {
    // console.log('edit prep 2')
    let newData = `
        <p>username: ${username.value}</p>
        <p>password: ${password.value}</p>
        <p>first name: ${firstName.value}</p>
        <p>last name: ${lastName.value}</p>
        <p>email: ${email.value}</p>
    `

    editconfText.innerHTML = `
    <div class="col-6">
        <h5>OLD DATA :</h5>
        <p>${oldData}</p>
    </div>
    <div class="col-6">
        <h5>NEW DATA :</h5>
        <div>${newData}</div>
    </div>
    `
}

editData = () => {    
    if(clickedId == 1){
        alert(`You can't modify admin data !`)
        return
    }
    let payload = {
        username: username.value,
        password: password.value,
        email: email.value,
        firstName: firstName.value,
        lastName: lastName.value
    }    
    fetch(`${BASE_URL}/api/v1/users/${clickedId}`, {
        method: "PUT",
        headers: {
            "content-type": "application/json"
        },
        body: JSON.stringify(payload)
    })
        .then(response => {
            loadData()
        })
}

loadData()

//================================== USER SECTION END ================================================

//================================== HISTORY SECTION START ================================================
loadHistory = () => {
    fetch(`${BASE_URL}/api/v1/history`, {
        method: "GET"
    }).then(res => res.json())
    .then(data => {
        let body = document.getElementById("body")
        body.innerHTML = ''
        for (let i in data) {
            let history = data[i]
            let date = new Date(history.createdAt.replace(' ', 'T'))
            body.innerHTML += `<tr>
                <td>${date}</td>                      
                <td>${history.userid}</td>                                       
                <td>${history.userdata.username}</td>                                       
                <td>${history.result}</td>                                       
                <td>                    
                    <a href="${BASE_URL}/api/v1/history/${history.id}" class="delete">Delete</a>
                </td>  
            </tr>`
        }
    })
}

document.body.addEventListener('click', (e) => {
    if (e.target.classList.contains("delete")) {
        e.preventDefault()
        fetch(e.target.href, {
            method: "DELETE"
        }).then(() => {
            loadHistory()
        })
    }
})

loadHistory()

//================================== HISTORY SECTION END ================================================