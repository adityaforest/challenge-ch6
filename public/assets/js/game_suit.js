//================================= SERVER COMMUNICATION ==========================================================
const BASE_URL = 'http://localhost:3000'

uploadHistory = () => {
    fetch(`${BASE_URL}/games/rockpaperscissor/user`)
    .then(res => res.json())
    .then(res => {
        console.log(res.id)
        let payload = {
            userid: res.id,
            result: result
        }
        fetch(`${BASE_URL}/api/v1/history`, {
            method: "POST",
            headers: {
                "content-type": "application/json"
            },
            body: JSON.stringify(payload)
        })
    })        
}

// const user = getUser()

// uploadHistory = (result) => {
//     let payload = {
//         userid: user.id,
//         result: result
//     }
//     fetch(`${BASE_URL}/api/v1/history`, {
//         method: "POST",
//         headers: {
//             "content-type": "application/json"
//         },
//         body: JSON.stringify(payload)
//     })
// }

//============================= CHAPTER KEMAREN2 =================================
var isPlayerAlreadyChoose = false;
var com, player;
var resultAudio;
var result;
const WARNA_BG_VIS = "#c4c4c4";
const WARNA_BG_INVIS = "#c4c4c400";

//============================== CLASSES =========================================
// NOTE : AGAK BERTELE2 YANG PENTING INHERIT , POLYMORPH ADA SEMUA :')
class Hand {
    constructor(name, weakness, advantage) {
        if (this.constructor === Hand) {
            throw new Error("Hand is an abstract class");
        }
        this.name = name;
        this.weakness = weakness;
        this.advantage = advantage;
    }
}

class Gunting extends Hand {
    constructor() {
        super("Gunting", "Batu", "Kertas");
    }
}

class Batu extends Hand {
    constructor() {
        super("Batu", "Kertas", "Gunting");
    }
}

class Kertas extends Hand {
    constructor() {
        super("Kertas", "Gunting", "Batu");
    }
}

class User {
    constructor(chosenChoice) {
        if (this.constructor === User) {
            throw new Error("User is an abstract class");
        }
        this.chosenChoice = chosenChoice;
    }

    chosen() {
        return this.chosenChoice;
    }
}

class Player extends User {
    constructor(chosenChoice) {
        super(chosenChoice);
    }

    choose(x) {
        this.chosenChoice = x;
    }

    fight(enemyChoice) {
        if (enemyChoice.name == player.chosen().advantage) {
            resultAudio = document.getElementById("audio-result-win");
            return "PLAYER WIN";
        }
        else if (enemyChoice.name == player.chosen().weakness) {
            resultAudio = document.getElementById("audio-result-lose");
            return "COM WIN";
        }
        else {
            resultAudio = document.getElementById("audio-result-draw");
            return "DRAW";
        }
    }
}

const Enemy = Base => class extends Base {
    startChoosing() {
        let choices = [new Batu(), new Gunting(), new Kertas()];
        let result = choices[Math.floor(Math.random() * choices.length)]
        this.chosenChoice = result;
    }
}

class Computer extends Enemy(User) {
    constructor(chosenChoice) {
        super(chosenChoice);
    }
}

//============================= FUNCTIONS ============================================
function playerChoose(clickedChoice) {
    if (isPlayerAlreadyChoose) return;
    isPlayerAlreadyChoose = true;

    document.getElementById("audio-choose").play();

    player = new Player();
    player.choose(clickedChoice);
    console.log("PLAYER : " + player.chosen().name);

    com = new Computer();
    com.startChoosing();
    console.log("COM : " + com.chosen().name);

    result = player.fight(com.chosen());
    document.getElementById("result-text").innerHTML = result;
    console.log("RESULT : " + result);

    startComChoosingAnimation();
}

function clickBatu() {
    playerChoose(new Batu());
}

function clickKertas() {
    playerChoose(new Kertas());
}

function clickGunting() {
    playerChoose(new Gunting());
}

function refreshPage() {
    location.reload();
}

//====================== ANIMATION BIAR KAYAK WEB JUDI ONLINE ==================================
var i = 0,
    bgArray = [
        document.getElementById("batu-com"),
        document.getElementById("kertas-com"),
        document.getElementById("gunting-com")
    ];

function startComChoosingAnimation() {
    document.getElementById("roulette-audio-opening").play();
    animationStart(100, 69, fastToSlow);
}

function animationStart(fractionTime, multiplier, afterState) {
    var interval = setInterval(transitionActivity, fractionTime);
    setTimeout(function () {
        clearInterval(interval);
        afterState();
    }, fractionTime * multiplier);
}

function transitionActivity() {
    let x = i - 1;
    if (x < 0) x = bgArray.length - 1;
    if (i > bgArray.length - 1) i = 0;

    bgArray[i].style.backgroundColor = WARNA_BG_VIS;
    bgArray[x].style.backgroundColor = WARNA_BG_INVIS;

    i++;
}

function fastToSlow() {
    if (com.chosen().name == "Batu") {
        animationStart(500, 4, showResult);
        document.getElementById("roulette-audio-endingLong").play();
    }
    else if (com.chosen().name == "Kertas") {
        animationStart(500, 2, showResult);
        document.getElementById("roulette-audio-endingShort").play();
    }
    else {
        animationStart(500, 3, showResult);
        document.getElementById("roulette-audio-endingMed").play();
    }
}

function showResult() {
    resultAudio.play();
    document.getElementById("versus-showcase").style.display = "none";
    document.getElementById("result-showcase").style.display = "flex";
    gsap.fromTo('#result-showcase', { opacity: 0, scale: 3, rotation: '180deg' }, { opacity: 1, scale: 1, rotation: '-30deg' });
    uploadHistory(result);
}

//============================== EVENTLISTENER FOR BUTTON MOUSEOVER AND MOUSEOUT =============================================

document.getElementById("batu-player").addEventListener("mouseover", function () {
    if (!isPlayerAlreadyChoose) document.getElementById("batu-player").style.backgroundColor = WARNA_BG_VIS;
});

document.getElementById("batu-player").addEventListener("mouseout", function () {
    if (!isPlayerAlreadyChoose) document.getElementById("batu-player").style.backgroundColor = WARNA_BG_INVIS;
});

document.getElementById("kertas-player").addEventListener("mouseover", function () {
    if (!isPlayerAlreadyChoose) document.getElementById("kertas-player").style.backgroundColor = WARNA_BG_VIS;
});

document.getElementById("kertas-player").addEventListener("mouseout", function () {
    if (!isPlayerAlreadyChoose) document.getElementById("kertas-player").style.backgroundColor = WARNA_BG_INVIS;
});

document.getElementById("gunting-player").addEventListener("mouseover", function () {
    if (!isPlayerAlreadyChoose) document.getElementById("gunting-player").style.backgroundColor = WARNA_BG_VIS;
});

document.getElementById("gunting-player").addEventListener("mouseout", function () {
    if (!isPlayerAlreadyChoose) document.getElementById("gunting-player").style.backgroundColor = WARNA_BG_INVIS;
});

