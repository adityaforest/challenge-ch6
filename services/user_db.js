const { UserGame, UserGameBiodata } = require('../models')

createUserData = async (data) => {
    // console.log(data)
    try {
        let x = await UserGame.create({
            username: data.username,
            password: data.password
        })
        let y = await UserGameBiodata.create({
            id: x.id,
            email: data.email,
            firstName: data.firstName,
            lastName: data.lastName
        })
    } catch (e) {
        console.log(`error: ${e.message}`)
    }
}

readUserData = async (id) => {
    let result = {}
    try {
        result = await UserGame.findOne({
            where: {
                id: id
            },
            include: [{
                model: UserGameBiodata,
                as: `biodata`,
                required: true
            }]
        })
    } catch (e) {
        console.log(`error: ${e.message}`)
    }
    return result
}

readAllUserData = async () => {
    let result = []
    try {
        result = await UserGame.findAll({
            include: [{
                model: UserGameBiodata,
                as: `biodata`,
                required: false
            }]
        })
    } catch (e) {
        console.log(`error: ${e.message}`)
    }
    return result
}

readUserDataByUsername = async (username) => {
    let result = {}
    try {
        result = await UserGame.findOne({
            where: {
                username: username
            },
            include: [{
                model: UserGameBiodata,
                as: `biodata`,
                required: true
            }]
        })
    } catch (e) {
        console.log(`error: ${e.message}`)
    }
    return result
}

updateUserData = async (id, data) => {
    try {
        let x = await UserGame.update({
            username: data.username,
            password: data.password
        }, {
            where: {
                id: id
            }
        })
        let y = await UserGameBiodata.update({
            email: data.email,
            firstName: data.firstName,
            lastName:data.lastName
        }, {
            where: {
                id: id
            }
        })
    } catch (e) {
        console.log(`error: ${e.message}`)
    }
}

deleteUserData = async (id) => {
    try {
        let x = await UserGame.destroy({
            where: { id: id }
        })
        let y = await UserGameBiodata.destroy({
            where: { id: id }
        })
    } catch (e) {
        console.log(`error: ${e.message}`)
    }
}

module.exports = {
    createUserData,
    readUserData,
    readAllUserData,
    readUserDataByUsername,
    updateUserData,
    deleteUserData
}