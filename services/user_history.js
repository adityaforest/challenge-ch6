const { UserGame, UserHistory ,sequelize} = require('../models')

createUserHistory = async (data) => {    
    try {
        await UserHistory.create({
            userid: data.userid,
            result: data.result
        })
    } catch (e) {
        console.log(`error: ${e.message}`)
    }
}

readAllUserHistory = async () => {
    let result = []
    try {
        result = await UserHistory.findAll({
            attributes: [
                'id',
                'userid',
                'result',
                'createdAt'
                // [sequelize.fn('DATE_FORMAT', sequelize.col('UserHistory.createdAt'), '%Y-%m-%d'), 'date']
                // [sequelize.fn('date_trunc','day',sequelize.col('UserHistory.createdAt')),'date']
            ],
            include: [{
                model: UserGame,
                as: `userdata`,
                attributes:['username'],
                required: false
                // where:{
                //     id:UserHistory.userid
                // }
            }],            
        })
    } catch (e) {
        console.log(`error: ${e.message}`)
    }
    return result
}

readUserHistoryByUserid = async (id) => {
    let result = []
    try {
        result = await UserHistory.findAll({ 
            where:{
                userid:id
            },
            include: [{
                model: UserGame,
                as: `userdata`,
                attributes:{
                    exclude:['password'],
                    include:['username']
                },
                required: false
            }],            
        })
    } catch (e) {
        console.log(`error: ${e.message}`)
    }
    return result
}

deleteUserHistory = async (id) => {
    try {
        let x = await UserHistory.destroy({
            where: { id: id }
        })        
    } catch (e) {
        console.log(`error: ${e.message}`)
    }
}

module.exports = {
    createUserHistory,
    readAllUserHistory,
    readUserHistoryByUserid,    
    deleteUserHistory
}